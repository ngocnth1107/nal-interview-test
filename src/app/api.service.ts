import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  //get All list articles
  public getAllArticles(){
    return this.httpClient.get(`https://5f55a98f39221c00167fb11a.mockapi.io/blogs`);
  }

  //get list articles follow pagination
  public getListArticles(page: number, limit: number){
    return this.httpClient.get(`https://5f55a98f39221c00167fb11a.mockapi.io/blogs?p=${page}&l=${limit}`);
  }

  //get articles detail
  public getArticleDetail(id: any){
    return this.httpClient.get(`https://5f55a98f39221c00167fb11a.mockapi.io/blogs/${id}`);
  }

  //get list articles by search
  public searchArticles(keywords: any){
    return this.httpClient.get(`https://5f55a98f39221c00167fb11a.mockapi.io/blogs/?search=${keywords}`);
  }

  //sort list articles by create date and desc or asc
  public sortCreateDate(order: any, page: number, limit: number){
    return this.httpClient.get(
      `https://5f55a98f39221c00167fb11a.mockapi.io/blogs/?sortBy=createdAt&order=${order}&p=${page}&l=${limit}`
      );
  }
}