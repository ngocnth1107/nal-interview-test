export interface ListArticles {
    id: string;
    createdAt: string;
    title: string;
    image: string;
    content: string;
}