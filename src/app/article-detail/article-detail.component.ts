import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  ID: any;
  articleDetail: any;

  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['id']) {
        this.ID = Number.parseInt(params['id']);
      }
    });
  }

  ngOnInit() {
    this.apiService.getArticleDetail(this.ID).subscribe((data) => {
      this.articleDetail = data;
    });
  }
}
