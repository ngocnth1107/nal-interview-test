import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
// import { ListArticles } from '../interface';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.css']
})
export class ListArticlesComponent implements OnInit {
  listArticles: any = [];
  allItems: any = [];
  detail: any;
  currentPage: number = 1;
  limitItem: number = 10;
  totalPage: number = 0;
  listPaging: any = [];
  value: string = '';

  constructor(private apiService: ApiService,
    private route: Router) { }

  ngOnInit() {
    this.getAllOfList();
    this.getListArticles();
  }

  getAllOfList() {
    //get all items to get length of the array
    //then take that length / for limit to get total page
    this.apiService.getAllArticles().subscribe((data) => {
      this.allItems = data;
      this.totalPage = Math.ceil(this.allItems.length / 10);
      for (let i = 1; i <= this.totalPage; i++) {
        this.listPaging.push({ "id": i });
      }
    });
  }

  //function get list articles with pagination
  getListArticles() {
    this.apiService.getListArticles(this.currentPage, this.limitItem).subscribe((data) => {
      this.listArticles = data;
    });
  }

  //function go to next page
  nextPage() {
    if (this.currentPage < this.totalPage) {
      this.currentPage += 1;
      this.getListArticles();
    }
  }

  //function go back pre page
  prePage() {
    if (this.currentPage > 1) {
      this.currentPage -= 1;
      this.getListArticles();
    }
  }

  //function go to page number
  handleNextPage(index: number) {
    this.currentPage = index + 1;
    this.getListArticles();
  }

  //function search
  startToSearch() {
    this.listPaging = [];
    if (this.value === '') {
      this.getAllOfList();
      this.getListArticles();
    } else {
        this.apiService.searchArticles(this.value).subscribe((data) => {
        this.listArticles = data;
        this.totalPage = Math.ceil(this.listArticles.length / 10);
        for (let i = 1; i <= this.totalPage; i++) {
          this.listPaging.push({ "id": i });
        }
      });
    }
  }

  //function sort by create date and desc or asc
  sortCreateDate(type: string) {
    this.listPaging = [];
    this.apiService.sortCreateDate(type, this.currentPage, this.limitItem).subscribe((data) => {
      this.listArticles = data;
      for (let i = 1; i <= this.totalPage; i++) {
        this.listPaging.push({ "id": i });
      }
    });
  }

  articleDetail(id: any) {
    let url = `article-detail?id=${id}`;
    this.route.navigateByUrl(url);
  }
}
